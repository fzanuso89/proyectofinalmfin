import numpy as np
import pandas as pd
import statsmodels.api as sm
from statsmodels.tsa.stattools import adfuller
import os

folder = os.getcwd()
df = pd.read_csv(folder + '/historic_csv/DLR_Normalize.csv')
df['date'] = pd.to_datetime(df['date'], format='%Y-%m-%d').dt.date
df.set_index('date', inplace=True)
df.drop('spot', axis=1, inplace=True)

# Fitting gamma to forward curve
gamma = np.full(df.shape[0], np.nan)
for t in range(df.shape[0]):
    idx = np.where(np.isfinite(df.iloc[t, :]))[0]
    idxDiff = np.array(list(idx[1:]-idx[:-1]))
    if (len(idx) >= 5) & (all(idxDiff[0:4] == 1)):
        FT = df.iloc[t, idx[:5]]
        T = sm.add_constant(range(FT.shape[0]))
        model = sm.OLS(np.log(FT.values), T)
        res = model.fit()
        gamma[t] = -12 * res.params[1]
results = adfuller(gamma[np.where(np.isfinite(gamma))], maxlag=1, regression='c', autolag=None)
print(results)
#(-4.586684184658408, 0.00013666960538551907, 1, 1995, {'1%': -3.4336320721769433, '5%': -2.862989840784964, '10%': -2.56754183359401})

# Calculating halfLife
gamma = pd.DataFrame(gamma)
gamma.fillna(method='ffill')
gammaGood = gamma[gamma.notna().values]
gammalag = gammaGood.shift()

deltaGamma = gammaGood - gammalag
deltaGamma = deltaGamma[1:]
gammalag = gammalag[1:]

X = sm.add_constant(gammalag)
model = sm.OLS(deltaGamma, X)
res = model.fit()

halflife = -np.log(2)/res.params[0]
# 41.095311903707795
print("half life: %s" % halflife)