# Estimating Spot and Roll Returns for ROFEX Dolar Future Using the Constant Returns Model

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import statsmodels.api as sm
import os
from datetime import datetime

folder = os.getcwd()
df = pd.read_csv(folder + '/historic_csv/DLR_Normalize.csv')
df['date'] = pd.to_datetime(df['date'], format='%Y-%m-%d').dt.date
df.set_index('date', inplace=True)

# Find spot prices
spot = df['spot']
df.drop('spot', axis=1, inplace=True)
T = sm.add_constant(range(spot.shape[0]))
model = sm.OLS(np.log(spot), T)
res = model.fit()
# print(res.summary())
print('Average annualized spot return:', 252 * res.params[1])
# Average annualized spot return: 0.16435486557297577

# Fitting gamma to forward curve
gamma = np.full(df.shape[0], np.nan)
for t in range(df.shape[0]):
    idx = np.where(np.isfinite(df.iloc[t, :]))[0]
    idxDiff = np.array(list(idx[1:] - idx[:-1]))
    if (len(idx) >= 5) and all(idxDiff[0:4] == 1):
        FT = df.iloc[t, idx[:5]]
        T = sm.add_constant(range(FT.shape[0]))
        model = sm.OLS(np.log(FT.values), T)
        res = model.fit()
        gamma[t] = -12 * res.params[1]


fig, ax = plt.subplots(figsize=(8, 6))
ax.plot(df.index, gamma, 'k-', label="gamma")
ax.plot(df.index, np.zeros(df.shape[0]), 'r--')
ax.set_ylabel('\u03B3')
ax.set_xlabel('time')
ax.yaxis.set_ticks(np.arange(-1.5, 1, 0.1))

fig.suptitle('Roll Return ROFEX Dolar')

plt.show()

print('Average annualized roll return:', np.nanmean(gamma))
# Average annualized roll return: -0.1752652392871127



# Show how Scatter Plot to see forward curve
fig, ax = plt.subplots(figsize=(8, 6))
d = '2019-03-11'
date = datetime.strptime(d, "%Y-%m-%d")
t = df.index.get_loc(date.date())
idx = np.where(np.isfinite(df.iloc[t, :]))[0]
FT = df.iloc[t, idx[:5]]
T = sm.add_constant(range(FT.shape[0]))
model = sm.OLS(np.log(FT.values), T)
res = model.fit()

x = range(FT.shape[0])
y = np.log(FT.values)

ax.plot(x, y, 'ko', label="scatterplot")
ax.plot(x, res.fittedvalues, 'r--.', label="OLS")

ax.set_ylabel('futures log price')
ax.set_xlabel('Maturity')
ax.set_xticklabels(["F1", "F2", "F3", "F4", "F5"])
ax.xaxis.set_ticks(np.arange(0, 5, 1))
fig.suptitle('ScatterPlot Futuro Dolar ROFEX')

plt.show()