# Finding Correlations between Returns of Different Time Frames for DLR

import os

import pandas as pd
from scipy.stats.stats import pearsonr

folder = os.getcwd()
df = pd.read_csv(folder + '/historic_csv/DLR_Normalize.csv')
df['date'] = pd.to_datetime(df['date'], format='%Y-%m-%d').dt.date
df.set_index('date', inplace=True)
df = df["spot"]

lookback_set = [1, 5, 10, 25, 60, 120, 250]
holddays_set = [1, 5, 10, 25, 60, 120, 250]

for lookback in lookback_set:
    for holddays in holddays_set:
        ret_lag = df.pct_change(periods=lookback)
        ret_fut = df.shift(-holddays).pct_change(periods=holddays)
        if lookback >= holddays:
            indepSet = range(0, ret_lag.shape[0], holddays)
        else:
            indepSet = range(0, ret_lag.shape[0], lookback)

        ret_lag = ret_lag.iloc[indepSet]
        ret_fut = ret_fut.iloc[indepSet]

        goodDates = (ret_lag.notna() & ret_fut.notna()).values
        (cc, pval) = pearsonr(ret_lag[goodDates], ret_fut[goodDates])
        print('%4i %4i %7.4f %7.4f' % (lookback, holddays, cc, pval))
