# Code to generate content used in final project doc

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

# Contango
fig, ax = plt.subplots(figsize=(8, 6))

x_points = [[1, 0], [2, 0], [3, 0], [4, 0]]
y_points = [[0, 1], [0, 2], [0, 3], [0, 4]]

for i in range(len(x_points)):
    x_values = [x_points[i][0], y_points[i][0]]
    y_values = [x_points[i][1], y_points[i][1]]
    ax.plot(x_values, y_values, 'k-.')
ax.set_ylabel('futures log price')
ax.set_xlabel('time')

# Turn off tick labels
ax.set_yticklabels(['log(spot)', 'log(P1)', 'log(P2)', 'log(P3)', 'log(P4)'])
ax.set_xticklabels(["t0", "t1", "t2", "t3", "t4"])
ax.xaxis.set_ticks(np.arange(0, 5, 1))
ax.yaxis.set_ticks(np.arange(0, 5, 1))

fig.suptitle('Contango')
plt.show()

fig, ax = plt.subplots(figsize=(8, 6))

x = [0, 1, 2, 3, 4]
y = [0, 1, 2, 3, 4]

ax.plot(x, y, 'ko', label="scatterplot")
ax.plot(x, y, 'r-', label="scatterplot")


ax.set_ylabel('futures log price')
ax.set_xlabel('Maturity')

# Turn off tick labels
ax.set_yticklabels(['log(P4)', 'log(P3)', 'log(P2)', 'log(P1)', 'log(spot)'])
ax.set_xticklabels(["Spot", "F1", "F2", "F3", "F4"])
ax.xaxis.set_ticks(np.arange(0, 5, 1))
ax.yaxis.set_ticks(np.arange(0, 5, 1))

fig.suptitle('Curva Forward en Contango')
plt.show()

# Backwardation
fig, ax = plt.subplots(figsize=(8, 6))

x_points = [[1, 0], [2, 0], [3, 0], [4, 0]]
y_points = [[0, -1], [0, -2], [0, -3], [0, -4]]

for i in range(len(x_points)):
    x_values = [x_points[i][0], y_points[i][0]]
    y_values = [x_points[i][1], y_points[i][1]]
    ax.plot(x_values, y_values, 'k-.')
ax.set_ylabel('futures log price')
ax.set_xlabel('time')

# Turn off tick labels
ax.set_yticklabels(['log(P4)', 'log(P3)', 'log(P2)', 'log(P1)', 'log(spot)'])
ax.set_xticklabels(["t0", "t1", "t2", "t3", "t4"])
ax.xaxis.set_ticks(np.arange(0, 5, 1))
ax.yaxis.set_ticks(np.arange(-4, 1, 1))

fig.suptitle('Backwardation')
plt.show()

fig, ax = plt.subplots(figsize=(8, 6))

x = [0, 1, 2, 3, 4]
y = [0, -1, -2, -3, -4]

ax.plot(x, y, 'ko', label="scatterplot")
ax.plot(x, y, 'r-', label="scatterplot")


ax.set_ylabel('futures log price')
ax.set_xlabel('Maturity')

# Turn off tick labels
ax.set_yticklabels(['log(P4)', 'log(P3)', 'log(P2)', 'log(P1)', 'log(spot)'])
ax.set_xticklabels(["Spot", "F1", "F2", "F3", "F4"])
ax.xaxis.set_ticks(np.arange(0, 5, 1))
ax.yaxis.set_ticks(np.arange(-4, 1, 1))

fig.suptitle('Curva Forward en Backwardation')
plt.show()