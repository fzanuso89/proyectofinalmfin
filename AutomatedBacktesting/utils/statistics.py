from numpy import cumsum, log, polyfit, sqrt, std, subtract
from numpy.random import randn

from statsmodels.tsa.tsatools import lagmat
from statsmodels.regression.linear_model import OLS
import statsmodels.formula.api as smf
from statsmodels.tsa.stattools import adfuller
import pandas as pd
import numpy as np


def moving_avg(df, windows):
    df.rolling(window=windows).mean()


def half_life_2(df, col):
    df['ylag'] = df[col].shift(1)
    df['deltaY'] = df[col] - df['ylag']
    results = smf.ols('deltaY ~ ylag', data=df).fit()
    lam = results.params['ylag']
    hl = -np.log(2)/lam
    return lam, hl


def half_life(ts):
    """
    Calculates the half life of a mean reversion
    """
    # make sure we are working with an array, convert if necessary
    ts = np.asarray(ts)
    print(ts)
    # delta = p(t) - p(t-1)
    delta_ts = np.diff(ts)

    # calculate the vector of lagged values. lag = 1
    lag_ts = np.vstack([ts[1:], np.ones(len(ts[1:]))]).T

    # calculate the slope of the deltas vs the lagged values
    beta = np.linalg.lstsq(lag_ts, delta_ts, rcond=None)

    # compute and return half life
    return (np.log(2) / beta[0])[0]


def hurst(ts):
    """
    Returns the Hurst Exponent of the time series vector ts
    """
    # make sure we are working with an array, convert if necessary
    ts = np.asarray(ts)

    # Helper variables used during calculations
    lagvec = []
    tau = []
    # Create the range of lag values
    max_lags = 100 if 100 < len(ts) else int(len(ts)/2)
    lags = range(2, max_lags)

    #  Step through the different lags
    for lag in lags:
        #  produce value difference with lag
        pdiff = np.subtract(ts[lag:],ts[:-lag])
        #  Write the different lags into a vector
        lagvec.append(lag)
        #  Calculate the variance of the difference vector
        tau.append(np.sqrt(np.std(pdiff)))

    #  linear fit to double-log graph
    m = np.polyfit(np.log10(np.asarray(lagvec)),
                   np.log10(np.asarray(tau).clip(min=0.0000000001)),
                   1)
    # return the calculated hurst exponent
    return m[0]*2.0


def variance_ratio(ts, lag=2):
    """
    Returns the variance ratio test result
    """
    # make sure we are working with an array, convert if necessary
    ts = np.asarray(ts)

    # Apply the formula to calculate the test
    n = len(ts)
    mu = sum(ts[1:n] - ts[:n - 1]) / n
    m = (n - lag + 1) * (1 - lag / n)
    b = sum(np.square(ts[1:n] - ts[:n - 1] - mu)) / (n - 1)
    t = sum(np.square(ts[lag:n] - ts[:n - lag] - lag * mu)) / m
    return t / (lag * b)


def adf(ts, maxlag=1):
    # make sure we are working with an array, convert if necessary
    ts = np.asarray(ts)
    return adfuller(ts, maxlag=maxlag)


def cadf(x, y, lags=1):
    """
    Returns the result of the Cointegrated Augmented Dickey-Fuller Test
    """
    # Calculate optimal hedge ratio "beta"
    res = OLS(y=y, x=x)
    beta_hr = res.beta.x

    # Calculate the residuals of the linear combination
    res = y - beta_hr * x

    # Calculate and output the CADF test on the residuals
    cadf = adfuller(res)
    return cadf

    # Calculate the linear regression between the two time series
    ols_result = OLS(x, y).fit()

    # Augmented Dickey-Fuller unit root test
    return adf(ols_result.resid, lags)


def johansen(ts, lags):
    """
    Calculate the Johansen Test for the given time series
    """
    # Make sure we are working with arrays, convert if necessary
    ts = np.asarray(ts)

    # nobs is the number of observations
    # m is the number of series
    nobs, m = ts.shape

    # Obtain the cointegrating vectors and corresponding eigenvalues
    eigenvectors, eigenvalues = maximum_likelihood_estimation(ts, lags)

    # Build table of critical values for the hypothesis test
    critical_values_string = """2.71   3.84    6.63
             13.43   15.50   19.94
             27.07   29.80   35.46
             44.49   47.86   54.68
             65.82   69.82   77.82
             91.11   95.75   104.96
             120.37  125.61  135.97
             153.63  159.53  171.09
             190.88  197.37  210.06
             232.11  239.25  253.24
             277.38  285.14  300.29
             326.53  334.98  351.25"""
    select_critical_values = np.array(
        critical_values_string.split(),
        float).reshape(-1, 3)
    critical_values = select_critical_values[:, 1]

    # Calculate numbers of cointegrating relations for which
    # the null hypothesis is rejected
    rejected_r_values = []
    for r in range(m):
        if h_test(eigenvalues, r, nobs, lags, critical_values):
            rejected_r_values.append(r)

    return eigenvectors, rejected_r_values


def h_test(eigenvalues, r, nobs, lags, critical_values):
    """
    Helper to execute the hypothesis test
    """
    # Calculate statistic
    t = nobs - lags - 1
    m = len(eigenvalues)
    statistic = -t * np.sum(np.log(np.ones(m) - eigenvalues)[r:])

    # Get the critical value
    critical_value = critical_values[m - r - 1]

    # Finally, check the hypothesis
    if statistic > critical_value:
        return True
    else:
        return False


def maximum_likelihood_estimation(ts, lags):
    """
    Obtain the cointegrating vectors and corresponding eigenvalues
    """
    # Make sure we are working with array, convert if necessary
    ts = np.asarray(ts)

    # Calculate the differences of ts
    ts_diff = np.diff(ts, axis=0)

    # Calculate lags of ts_diff.
    ts_diff_lags = lagmat(ts_diff, lags, trim='both')

    # First lag of ts
    ts_lag = lagmat(ts, 1, trim='both')

    # Trim ts_diff and ts_lag
    ts_diff = ts_diff[lags:]
    ts_lag = ts_lag[lags:]

    # Include intercept in the regressions
    ones = np.ones((ts_diff_lags.shape[0], 1))
    ts_diff_lags = np.append(ts_diff_lags, ones, axis=1)

    # Calculate the residuals of the regressions of diffs and lags
    # into ts_diff_lags
    inverse = np.linalg.pinv(ts_diff_lags)
    u = ts_diff - np.dot(ts_diff_lags, np.dot(inverse, ts_diff))
    v = ts_lag - np.dot(ts_diff_lags, np.dot(inverse, ts_lag))

    # Covariance matrices of the calculated residuals
    t = ts_diff_lags.shape[0]
    Svv = np.dot(v.T, v) / t
    Suu = np.dot(u.T, u) / t
    Suv = np.dot(u.T, v) / t
    Svu = Suv.T

    # ToDo: check for singular matrices and exit
    Svv_inv = np.linalg.inv(Svv)
    Suu_inv = np.linalg.inv(Suu)

    # Calculate eigenvalues and eigenvectors of the product of covariances
    cov_prod = np.dot(Svv_inv, np.dot(Svu, np.dot(Suu_inv, Suv)))
    eigenvalues, eigenvectors = np.linalg.eig(cov_prod)

    # Use Cholesky decomposition on eigenvectors
    evec_Svv_evec = np.dot(eigenvectors.T, np.dot(Svv, eigenvectors))
    cholesky_factor = np.linalg.cholesky(evec_Svv_evec)
    eigenvectors = np.dot(eigenvectors, np.linalg.inv(cholesky_factor.T))

    # Order the eigenvalues and eigenvectors
    indices_ordered = np.argsort(eigenvalues)
    indices_ordered = np.flipud(indices_ordered)

    # Return the calculated values
    return eigenvalues[indices_ordered], eigenvectors[:, indices_ordered]

