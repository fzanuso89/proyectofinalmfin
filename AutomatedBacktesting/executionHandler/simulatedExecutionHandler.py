#coding=utf-8

import datetime

from AutomatedBacktesting.event.fillEvent import FillEvent
from .executionHandler import ExecutionHandler


class SimulatedExecutionHandler(ExecutionHandler):
    """
    The simulated execution handler simply converts all order
    objects into their equivalent fill objects automatically
    without latency, slippage or fill-ratio issues.
    This allows a straightforward "first go" test of any strategies,
    before implementation with a more sophisticated execution
    handler.
    """
    def __init__(self, events):
        """
        Initialises the handler, setting the event queues
        up internally.
        Parameters:
        events - The Queue of Event objects.
        """
        self.events = events

    def execute_order(self, event):
        """
        Simply converts Order objects into Fill objects naively,
        i.entries. without any latency, slippage or fill ratio problems.
        Parameters:
        event - Contains an Event object with order information.
        """
        if event.type == "ORDER":
            fill_event = FillEvent(datetime.datetime.utcnow(), event.symbol, "ROFEX", event.quantity, event.direction, 0, 0)
        self.events.put(fill_event)