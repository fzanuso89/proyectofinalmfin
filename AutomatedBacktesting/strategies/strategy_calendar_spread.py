#coding=utf-8

import time
from datetime import datetime
from dateutil.relativedelta import relativedelta

from enum import Enum
import pandas as pd
import numpy as np
import statsmodels.api as sm

from .strategy import Strategy
from AutomatedBacktesting.event.signalEvent import SignalEvent


class StateCalendarSpreadStrategy(Enum):
    BUY = 0
    HOLDING = 1


class CalendarSpreadStrategy(Strategy):

    def __init__(self, data_handler, events, params):
        self.strategy_id = "CalendarSpreadStrategy"
        self.data_handler = data_handler
        self.events = events
        self.state = StateCalendarSpreadStrategy.BUY
        self.holding_period = params[0]
        self.lookback = params[1]
        self.days_before_exp = params[2]
        self.spread_month = params[3]
        self.underlying = params[4]
        self.near_position = None
        self.far_position = None
        self.holding_days = 0
        self.df_history = pd.DataFrame(columns=[], index=pd.to_datetime([]))
        self.gamma = np.array([])
        self.position = [0, 0]

        self.count = dict()
        self.timer = dict()
        self.timers_names = ["update_gamma_dataframe"]
        self._init_timer()

    def print_timer(self):
        for name in self.timers_names:
            mean = 0 if self.count[name] == 0 else (self.timer[name] / self.count[name])
            print("case %s - count: %s - Time -> total: %s - mean: %s" %
                  (name, self.count[name], self.timer[name], mean))

    def _init_timer(self):
        for name in self.timers_names:
            self.count[name] = 0
            self.timer[name] = 0.0

    def timeit(self, method, param, name):
        start = time.process_time()
        if param:
            method(param)
        else:
            method()
        self.timer[name] += time.process_time() - start
        self.count[name] += 1

    def calculate_signals(self, event):

        self.timeit(self.update_gamma_dataframe, None, 'update_gamma_dataframe')

        if self.state is StateCalendarSpreadStrategy.BUY:
            if self.check_days():
                long_future, short_future = self.get_contracts_to_trade()
                if long_future and short_future:
                    qty_near, qty_far = self.get_qty()
                    if qty_near and qty_far:
                        self.near_position = SignalEvent(self.strategy_id, long_future, datetime.now(),
                                                         self.get_side(qty_near), abs(qty_near))
                        self.far_position = SignalEvent(self.strategy_id, short_future, datetime.now(),
                                                        self.get_side(qty_far), abs(qty_far))
                        self.events.put(self.near_position)
                        self.events.put(self.far_position)
                        self.position = [qty_near, qty_far]
                        self.state = StateCalendarSpreadStrategy.HOLDING
        elif self.state is StateCalendarSpreadStrategy.HOLDING:
            self.holding_days += 1
            if self.holding_days > self.holding_period: #roll position
                self.holding_days = 0
                exit_near_position = SignalEvent(self.strategy_id, self.near_position.symbol, datetime.now(), "EXIT")
                exit_far_position = SignalEvent(self.strategy_id, self.far_position.symbol, datetime.now(), "EXIT")
                self.events.put(exit_near_position)
                self.events.put(exit_far_position)
                self.position = [0, 0]
                long_future, short_future = self.get_contracts_to_trade()
                if long_future and short_future:
                    qty_near, qty_far = self.get_qty()
                    if qty_near and qty_far:
                        self.near_position = SignalEvent(self.strategy_id, long_future, datetime.now(),
                                                         self.get_side(qty_near), abs(qty_near))
                        self.far_position = SignalEvent(self.strategy_id, short_future, datetime.now(),
                                                        self.get_side(qty_far), abs(qty_far))
                        self.events.put(self.near_position)
                        self.events.put(self.far_position)
                        self.position = [qty_near, qty_far]
                else:
                    self.state = StateCalendarSpreadStrategy.BUY
            else:
                qty_near, qty_far = self.get_qty()
                if qty_near and qty_far:
                    if self.near_position.quantity != qty_near and self.far_position.quantity != qty_far:
                        self.near_position = SignalEvent(self.strategy_id, self.near_position.symbol, datetime.now(),
                                                         self.get_side(qty_near), abs(qty_near) + abs(self.position[0]))
                        self.far_position = SignalEvent(self.strategy_id, self.far_position.symbol, datetime.now(),
                                                        self.get_side(qty_far), abs(qty_far) + abs(self.position[1]))
                        self.events.put(self.near_position)
                        self.events.put(self.far_position)
                        self.position = [qty_near, qty_far]

    def check_days(self):
        if self.df_history.shape[0] > self.lookback:
            return True
        return False

    def get_contracts_to_trade(self):
        contracts = self.sort_contracts(self.data_handler.get_active_symbols())
        days_needed = self.holding_period + self.days_before_exp
        for contract in contracts:
            days_to_expire = self.data_handler.get_trading_days_to_expire(contract)
            if days_to_expire >= days_needed:
                next_contract = self.get_next_contract(contract)
                if next_contract in contracts:
                    return contract, next_contract
        #print("No pudimos encontrar 2 contractos para ejecutar la estrategia.")
        return None, None

    def get_qty(self):
        gamma = pd.DataFrame(self.gamma)
        gamma = gamma.fillna(method='ffill')
        ma = gamma.rolling(self.lookback).mean()
        mstd = gamma.rolling(self.lookback).std()
        z_score = (gamma - ma) / mstd
        if z_score.iloc[-1][0] > 0:
            return 1, -1
        elif z_score.iloc[-1][0] < 0:
            return -1, 1
        else:
            print("error al calcular zscore")
            return None, None

    def sort_contracts(self, contracts):
        dates_index = dict()
        dates = []
        for contract in contracts:
            date = self.data_handler.get_contract_exp_date(contract)
            dates.append(date)
            dates_index[date] = contract
        dates.sort()
        sorted_contracts = []
        for date in dates:
            sorted_contracts.append(dates_index[date])
        return sorted_contracts

    def get_next_contract(self, contract):
        month = int(contract[3:5])
        year = int(contract[5:9])
        new = datetime(year, month, 1) + relativedelta(months=self.spread_month)
        month = str(new.month) if new.month >= 10 else "0" + str(new.month)
        name = str(self.underlying) + month + str(new.year)
        return name

    def update_gamma_dataframe(self):
        contracts = self.sort_contracts(self.data_handler.get_active_symbols())
        self.df_history = self.df_history.append(pd.DataFrame(index=[self.data_handler.get_day()]))
        for contract in contracts:
            if contract not in self.df_history.columns:
                self.df_history[contract] = np.nan
            self.df_history.loc[self.data_handler.get_day()][contract] = \
                self.data_handler.get_latest_bar_value(contract, 'settlement')
        #calculate gamma
        t = len(self.gamma)
        self.gamma = np.append(self.gamma, [np.nan], axis=0)
        idx = np.where(np.isfinite(self.df_history.iloc[t, :]))[0]
        idxDiff = np.array(list(idx[1:] - idx[:-1]))
        if (len(idx) >= 5) & (all(idxDiff[0:4] == 1)):
            FT = self.df_history.iloc[t, idx[:5]]
            T = sm.add_constant(range(FT.shape[0]))
            model = sm.OLS(np.log(FT.values), T)
            res = model.fit()
            self.gamma[t] = -12 * res.params[1]

    def get_side(self, size):
        if size > 0:
            return "LONG"
        elif size < 0:
            return "SHORT"
        else:
            return None

    def get_opposite_side(self, side):
        if side == "SHORT":
            return "LONG"
        elif side == "LONG":
            return "SHORT"
        else:
            return None
