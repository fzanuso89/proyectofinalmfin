from .strategy import Strategy
from AutomatedBacktesting.event.signalEvent import SignalEvent

from enum import Enum
from datetime import datetime

class StateDayMomentumStrategy(Enum):
    TRADE = 0
    HOLD = 1


class DayMomentumStrategy(Strategy):

    def __init__(self, data_handler, events, params):
        self.strategy_id = "DayMomentumStrategy"
        self.data_handler = data_handler
        self.events = events
        self.state = StateDayMomentumStrategy.TRADE
        self.lookback_period = params[0]
        self.holding_period = params[1]
        self.underlying = params[2]
        self.holding_days = 0
        self.roll_period = 2
        self.use_near = False
        self.months = 12
        self.last_signal = None

    def calculate_signals(self, event):
        if self.state is StateDayMomentumStrategy.TRADE:
            if self.check_days():
                self.generate_signal()
        elif self.state is StateDayMomentumStrategy.HOLD:
            self.holding_days += 1
            if self.holding_days > self.holding_period: #roll position
                self.holding_days = 0
                exit_near_position = SignalEvent(self.strategy_id, self.last_signal.symbol, datetime.now(), "EXIT")
                self.events.put(exit_near_position)
                self.state = StateDayMomentumStrategy.TRADE
                self.generate_signal()
            elif self.use_near:
                contract = self.get_contract_to_trade()
                if contract != self.last_signal.symbol:
                    exit_near_position = SignalEvent(self.strategy_id, self.last_signal.symbol, datetime.now(), "EXIT")
                    self.events.put(exit_near_position)
                    self.last_signal = SignalEvent(self.strategy_id, contract, datetime.now(),
                                                   self.last_signal.signal_type, self.last_signal.quantity)
                    self.events.put(self.last_signal)

    def generate_signal(self):
        contract = self.get_contract_to_trade()
        if contract:
            side = self.get_side()
            if side:
                self.last_signal = SignalEvent(self.strategy_id, contract, datetime.now(), side, 1)
                self.events.put(self.last_signal)
                self.state = StateDayMomentumStrategy.HOLD

    def check_days(self):
        if self.data_handler.get_trading_days_history() > self.lookback_period:
            return True
        return False

    def get_contract_to_trade(self):
        contracts = self.sort_contracts(self.data_handler.get_active_symbols())
        return contracts[-1]
        for contract in contracts:
            days_to_expire = self.data_handler.get_trading_days_to_expire(contract)
            if days_to_expire > self.roll_period:
                return contract

    def get_side(self):
        data = self.data_handler.get_latest_bars_values(self.underlying, "spot", self.lookback_period)
        diff = data[-1] - data[0]
        if diff > 0:
            return "LONG"
        elif diff < 0:
            return "SHORT"
        else:
            return None

    def sort_contracts(self, contracts):
        dates_index = dict()
        dates = []
        for contract in contracts:
            date = self.data_handler.get_contract_exp_date(contract)
            dates.append(date)
            dates_index[date] = contract
        dates.sort()
        sorted_contracts = []
        for date in dates:
            sorted_contracts.append(dates_index[date])
        return sorted_contracts