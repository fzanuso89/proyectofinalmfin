#coding=utf-8

from __future__ import print_function
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from AutomatedBacktesting.event.orderEvent import OrderEvent
from .performance import create_sharpe_ratio, create_drawdowns, create_apr

try:
    import Queue as queue
except ImportError:
    import queue


class Portfolio(object):
    """
    The portfolio class handles the positions and market
    value of all instruments at a resolution of a "bar",
    i.entries. secondly, minutely, 5-min, 30-min, 60 min or EOD.
    The positions DataFrame stores a time-index of the
    quantity of positions held.
    The holdings DataFrame stores the cash and total market
    holdings value of each symbol for a particular
    time-index, as well as the percentage change in
    portfolio total across bars.
    """
    def __init__(self, bars, events, start_date, initial_capital=100000.0):
        """
        Initialises the portfolio with bars and an event queue.
        Also includes a starting datetime index and initial capital
        (USD unless otherwise stated).
        Parameters:
        bars - The dataHandler object with current market data.
        events - The Event Queue object.
        start_date - The start date (bar) of the portfolio.
        initial_capital - The starting capital in USD.
        """
        self.bars = bars
        self.events = events
        self.symbol_list = self.bars.all_symbol_list
        self.underlying_list = []
        for s in self.symbol_list:
            if s[:3] not in self.underlying_list:
                self.underlying_list.append(s[:3])
        self.start_date = start_date
        self.contracts_with_position = dict()
        self.initial_capital = initial_capital
        self.all_positions = self.construct_all_positions()
        self.current_positions = dict( (k,v) for k, v in [(s, 0) for s in self.symbol_list])
        for s in self.underlying_list:
            self.current_positions[s[:3]] = [0, 0]
        self.all_holdings = self.construct_all_holdings()
        self.current_holdings = self.construct_current_holdings()

    def construct_all_positions(self):
        """
        Constructs the positions list using the start_date
        to determine when the time index will begin.
        """

        d = dict((k, v) for k, v in [(s, 0) for s in self.symbol_list])
        d["datetime"] = self.start_date
        return [d]

    def construct_all_holdings(self):
        """
        Constructs the holdings list using the start_date
        to determine when the time index will begin.
        """

        d = dict((k, v) for k, v in [(s, 0.0) for s in self.symbol_list])
        d["datetime"] = self.start_date
        d["cash"] = self.initial_capital
        d["margin"] = 0.0
        d["commission"] = 0.0
        d["total"] = self.initial_capital
        return [d]

    def construct_current_holdings(self):
        """
        This constructs the dictionary which will hold the instantaneous
        value of the portfolio across all symbols.
        """

        d = dict((k, v) for k, v in [(s, 0.0) for s in self.symbol_list])
        d["cash"] = self.initial_capital
        d["margin"] = dict((k, v) for k, v in [(s, 0.0) for s in self.symbol_list])
        d["commission"] = 0.0
        d["total"] = self.initial_capital
        return d

    def update_timeindex(self, event):
        """
        Adds a new record to the positions matrix for the current
        market data bar. This reflects the PREVIOUS bar, i.entries. all
        current market data at this stage is known (OHLCV).
        Makes use of a MarketEvent from the events queue.
        """

        latest_datetime = self.bars.get_day()
        # Update positions
        # ================
        dp = dict((k, v) for k, v in [(s, 0) for s in self.symbol_list])
        dp["datetime"] = latest_datetime
        for s in self.symbol_list:
            dp[s] = self.current_positions[s]
        # Append the current positions
        self.all_positions.append(dp)
        # Update holdings
        # ===============
        dh = dict((k, v) for k, v in [(s, 0) for s in self.symbol_list])
        dh["datetime"] = latest_datetime
        dh["margin"] = 0.0
        for s in self.contracts_with_position:
            dh["margin"] += self.current_holdings["margin"][s]
        strategy_pos = 0
        if self.contracts_with_position:
            strategy_pos = (self.current_holdings["cash"] / (dh["margin"] * 0.5) // 2) * 2
        dh["commission"] = self.current_holdings["commission"]
        for s in self.contracts_with_position:
            # Approximation to the real value
            last_pxs = self.bars.get_latest_bars_values(s, "settlement", 2)
            if not np.any(np.isnan(last_pxs)):
                diff = (last_pxs[1] - last_pxs[0])
                market_to_market = self.current_positions[s] * diff * self.bars.get_contract_size(s)
                dh[s] += market_to_market
                self.current_holdings["cash"] += market_to_market
        dh["cash"] = self.current_holdings["cash"]
        dh["total"] = self.current_holdings["cash"]
        # Append the current holdings
        self.all_holdings.append(dh)

    def update_positions_from_fill(self, fill):
        """
        Takes a Fill object and updates the position matrix to
        reflect the new position.
        Parameters:
        fill - The Fill object to update the positions with.
        """
        # Check whether the fill is a buy or sell
        fill_dir = 0
        if fill.direction == "BUY":
            fill_dir = 1
        if fill.direction == "SELL":
            fill_dir = -1
        # Update positions list with new quantities
        self.current_positions[fill.symbol] += fill_dir*fill.quantity
        if fill_dir > 0:
            self.current_positions[fill.symbol[:3]][0] += fill.quantity
        if fill_dir < 0:
            self.current_positions[fill.symbol[:3]][1] += fill.quantity
        if self.current_positions[fill.symbol] == 0:
            del self.contracts_with_position[fill.symbol]
        elif fill.symbol not in self.contracts_with_position:
            self.contracts_with_position[fill.symbol] = 0

    def update_holdings_from_fill(self, fill):
        """
        Takes a Fill object and updates the holdings matrix to
        reflect the holdings value.
        Parameters:
        fill - The Fill object to update the holdings with.
        """

        # Check whether the fill is a buy or sell
        fill_dir = 0
        if fill.direction == "BUY":
            fill_dir = 1
        if fill.direction == "SELL":
            fill_dir = -1
        # Update holdings list with new quantities
        self.current_holdings["margin"][fill.symbol] = abs(self.current_positions[fill.symbol]) * \
                                                       self.bars.get_contract_size(fill.symbol) * \
                                                       self.bars.get_latest_bar_value(fill.symbol, "settlement") * \
                                                       0.1
        self.current_holdings["commission"] += fill.commission
        self.current_holdings["cash"] -= fill.commission

    def update_fill(self, event):
        """
        Updates the portfolio current positions and holdings
        from a FillEvent.
        """

        if event.type == "FILL":
            self.update_positions_from_fill(event)
        self.update_holdings_from_fill(event)

    def generate_naive_order(self, signal):
        """
        Simply files an Order object as a constant quantity
        sizing of the signal object, without risk management or
        position sizing considerations.
        Parameters:
        signal - The tuple containing Signal information.
        """

        order = None
        symbol = signal.symbol
        direction = signal.signal_type
        mkt_quantity = signal.quantity
        cur_quantity = self.current_positions[symbol]
        order_type = "MKT"
        if direction == "LONG":
            order = OrderEvent(symbol, order_type, mkt_quantity, "BUY")
        elif direction == "SHORT":
            order = OrderEvent(symbol, order_type, mkt_quantity, "SELL")
        elif direction == "EXIT" and cur_quantity > 0:
            order = OrderEvent(symbol, order_type, abs(cur_quantity), "SELL")
        elif direction == "EXIT" and cur_quantity < 0:
            order = OrderEvent(symbol, order_type, abs(cur_quantity), "BUY")
        return order

    def update_signal(self, event):
        """
        Acts on a SignalEvent to generate new orders
        based on the portfolio logic.
        """

        if event.type == "SIGNAL":
            order_event = self.generate_naive_order(event)
        self.events.put(order_event)

    def create_equity_curve_dataframe(self):
        """
        Creates a pandas DataFrame from the all_holdings
        list of dictionaries.
        """

        curve = pd.DataFrame(self.all_holdings)
        curve.set_index("datetime", inplace = True)
        curve["returns"] = curve["total"].pct_change()
        curve["equity_curve"] = (1.0 + curve["returns"]).cumprod()
        self.equity_curve = curve

    def print_equity_curve(self, s, e):
        fig, ax = plt.subplots(figsize=(8, 6))
        x = np.arange(0, self.equity_curve["equity_curve"].shape[0], 1)
        y = self.equity_curve["equity_curve"].values
        ax.plot(x, y, 'r-.')
        ax.set_ylabel('Capital')
        ax.set_xlabel('Desde %s hasta %s' % (s, e))
        fig.suptitle('Curva de Capital Estrategia Momentum')
        plt.show()

    def output_summary_stats(self):
        """
        Creates a list of summary statistics for the portfolio.
        """

        total_return = self.equity_curve["equity_curve"][-1]
        returns = self.equity_curve["returns"]
        pnl = self.equity_curve["equity_curve"]
        apr = create_apr(returns)
        sharpe_ratio = create_sharpe_ratio(returns)
        drawdown, max_dd, dd_duration = create_drawdowns(pnl)
        self.equity_curve["drawdown"] = drawdown
        stats = [("Total Return", "%0.2f%%" % \
                  ((total_return - 1.0) * 100.0)),
                 ("APR", "%0.2f" % apr),
                 ("Sharpe Ratio", "%0.2f" % sharpe_ratio),
                 ("Max Drawdown", "%0.2f%%" % (max_dd * 100.0)),
                 ("Drawdown Duration", "%d" % dd_duration)]
        self.equity_curve.to_csv("equity.csv")
        return stats
