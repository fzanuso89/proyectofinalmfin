#coding=utf-8

from .event import Event


class SignalEvent(Event):
    """
    Handles the event of sending a Signal from a strategies object.
    This is received by a portfolio object and acted upon.
    """
    def __init__(self, strategy_id, symbol, datetime, signal_type, qty=0):
        """
        Initialises the SignalEvent.
        Parameters:
        strategy_id - The unique identifier for the strategies that
        generated the signal.
        symbol - The ticker symbol, entries.g. ’GOOG’.
        datetime - The timestamp at which the signal was generated.
        signal_type - ’LONG’ or ’SHORT’.
        strength - An adjustment factor "suggestion" used to scale
        quantity at the portfolio level. Useful for pairs strategies.
        """
        self.type = "SIGNAL"
        self.strategy_id = strategy_id
        self.symbol = symbol
        self.datetime = datetime
        self.signal_type = signal_type
        self.quantity = qty