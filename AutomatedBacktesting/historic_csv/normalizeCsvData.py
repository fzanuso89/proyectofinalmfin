# Code to normalize .csv file obtain from rofex cem
# Output is an .csv with T x F matrix (T=number of days, F=number of futures + 1 column for the historic Spot)

import os
from datetime import datetime

import pandas as pd


def normalize_rofex_data(input_dir, output_dir, underlying):
    dateparse = lambda x: datetime.strptime(x, '%d/%m/%Y 12:00:00 a.m.')
    pd_data_history = pd.io.parsers.read_csv(os.path.join(input_dir, "%s_History.csv" % underlying), header=0,
                                             index_col=0, parse_dates=['date'], date_parser=dateparse, thousands=',',
                                             names=["date", "contract", "type", "Ejercicio", "open", "low", "high",
                                                    "close", "volume", "settlement", "discount"]).sort_values("date",
                                                                                                              ascending=True)
    pd_data_history.drop('type', axis=1, inplace=True)
    pd_data_history.drop('Ejercicio', axis=1, inplace=True)
    pd_data_history.drop('discount', axis=1, inplace=True)
    pd_data_history["open"] = pd_data_history["open"] / 1000
    pd_data_history["low"] = pd_data_history["low"] / 1000
    pd_data_history["high"] = pd_data_history["high"] / 1000
    pd_data_history["close"] = pd_data_history["close"] / 1000
    pd_data_history["settlement"] = pd_data_history["settlement"] / 1000000

    contract_list = pd_data_history["contract"].unique()
    pd_data_symbol = pd.DataFrame(index=pd_data_history.index.unique())
    contracts_unorder = dict()
    for contract in contract_list:
        if contract[:3] == underlying:
            contracts_unorder[int(contract.strip()[5:9]) * 100 + int(contract.strip()[3:5])] = contract.strip()
            pd_data_symbol = pd.merge(pd_data_symbol,
                                      pd_data_history[pd_data_history["contract"] == contract]["settlement"],
                                      how='outer', left_index=True, right_index=True)
            pd_data_symbol = pd_data_symbol.rename(columns={"settlement": contract.strip()})
    columns = []
    order = list(contracts_unorder.keys())
    order.sort()
    for priority in order:
        columns.append(contracts_unorder[priority])
    print("start:", order[0])
    pd_data_symbol = pd_data_symbol[columns]
    pd_data_spot = pd.io.parsers.read_csv(os.path.join(input_dir, "%s_Spot.csv" % underlying), header=0,
                                          index_col=0, parse_dates=['date'], date_parser=dateparse, thousands=',',
                                          names=["date", "type", "spot"]).sort_values("date", ascending=True)
    pd_data_spot["spot"] = pd_data_spot["spot"] / 1000000
    pd_data_symbol = pd.merge(pd_data_symbol,
                              pd_data_spot["spot"], how='inner',
                              left_index=True, right_index=True)
    pd_data_symbol.to_csv(output_dir + "/%s_Normalize.csv" % underlying)
    print(pd_data_symbol.head())


input_dir = os.getcwd() + '/historic_csv/'
output_dir = os.getcwd()
underlying = "DLR"

normalize_rofex_data(input_dir, output_dir, underlying)