#coding=utf-8

import os
import os.path
from datetime import datetime
import calendar
import math
import numpy as np
import pandas as pd
from copy import deepcopy

from .dataHandler import DataHandler
from AutomatedBacktesting.event.marketEvent import MarketEvent


class HistoricCSVDataHandler(DataHandler):
    """
    HistoricCSVDataHandler is designed to read CSV files for
    each requested symbol from disk and provide an interface
    to obtain the "latest" bar in a manner identical to a live
    trading interface.
    """
    def __init__(self, events, csv_dir, symbol_list, contract_sizes,
                 start_date=None, end_date=None, start_day_shift=None, logging=False):
        """
        Initialises the historic data handler by requesting
        the location of the CSV files and a list of symbols.
        It will be assumed that all files are of the form
        ’symbol.csv’, where symbol is a string in the list.
        Parameters:
        events - The Event Queue.
        csv_dir - Absolute directory path to the CSV files.
        symbol_list - A list of symbol strings.
        """
        self.events = events
        self.csv_dir = csv_dir
        self.logging = logging
        self.symbol_list = symbol_list
        self.all_symbol_list = symbol_list
        self.active_symbols = {}
        self.underlyings = []
        self.symbol_data = {}
        self.contract_size = contract_sizes
        self.latest_symbol_data = {}
        self.continue_backtest = True
        self.index = None
        self.index_now = None
        self.trading_days_history_count = 0
        self.start_day_shift = start_day_shift
        self._open_convert_csv_files()
        self._open_convert_csv_spot_files()
        self.index = pd.DataFrame(index=self.index)
        self._init_dates(start_date, end_date)
        self._init_handler()

    def reset(self, start_date, end_date):
        self.continue_backtest = True
        self.symbol_list = deepcopy(self.all_symbol_list)
        self.active_symbols = {}
        for c in self.latest_symbol_data.keys():
            self.latest_symbol_data[c] = []
        self._init_dates(start_date, end_date)
        self._init_handler()

    def _init_handler(self):
        if self.index_now > self.index.index[0]:
            dates_to_process = self.index[:self.index_now].index
            self.index_now = self.index.index[0]
            self._update_valid_symbol_list()
            self.log_if("Processing dates from %s to %s" % (dates_to_process[0].date(), dates_to_process[-1].date()))
            for i in range(len(dates_to_process)-1):
                self.next(False)
        elif self.start_day_shift and 0 < self.start_day_shift < len(self.index.index):
            for i in range(self.start_day_shift):
                self.next(False)
        else:
            self._update_valid_symbol_list()
            self._update_bars()

        aux = self.index.index[self.index.index >= self.index_now]
        aux = aux[self.end_date >= aux]
        print("Backtest start at: '%s' and ends at: '%s' - Trading Days: %s" %
              (self.index_now.date(), self.end_date.date(), len(aux)))

    def _init_dates(self, start_date, end_date):
        if end_date:
            try:
                end_date = datetime.strptime(end_date, "%Y-%m-%d")
                self.end_date = self.index.index[self.index.index <= end_date][-1]
            except ValueError:
                raise
        else:
            self.end_date = self.index.index[-1]
        if start_date:
            try:
                date = datetime.strptime(start_date, "%Y-%m-%d")
                self.index_now = self.index.index[self.index.index >= date][0]
            except ValueError:
                raise
        else:
            self.index_now = self.index.index[0]

    def _open_convert_csv_files_(self):
        """
        Opens the CSV files from the data directory, converting
        them into pandas DataFrames within a symbol dictionary.
        For this handler it will be assumed that the data is
        taken from ROFEX CEM. Thus its format will be respected.
        """
        symbols = dict()
        for s in self.symbol_list:
            if s[:3] not in symbols.keys():
                symbols[s[:3]] = []
            if len(s) > 3:
                symbols[s[:3]].append(s)
        self.symbol_list = []

        for underlying in symbols.keys():
            path = os.path.join(self.csv_dir, "%s_Normalize.csv" % underlying)
            pd_data_history = pd.io.parsers.read_csv(path, header=0, index_col=0,
                                                     parse_dates=['date']).sort_values("date", ascending=True)
            contract_list = pd_data_history.columns.tolist()[:-1]

            pd_data_symbol = dict()
            self.symbol_data[underlying] = pd_data_history["spot"]
            self.index = self.symbol_data[underlying].index if self.index is None else self.index.union(self.symbol_data[underlying].index)
            self.latest_symbol_data[underlying] = []

            if not symbols[underlying]:
                self.symbol_list.extend(contract_list)
            else:
                self.symbol_list.append(symbols[underlying])

            for contract in symbols[underlying]:
                if contract in contract_list:
                    self.symbol_data[contract] = pd_data_history[contract]
                    max_contract_date = self.get_contract_exp_date(contract)
                    indexToDelete = self.symbol_data[contract][self.symbol_data[contract].index > max_contract_date].index
                    # Delete these row indexes from dataFrame
                    self.symbol_data[contract] = self.symbol_data[contract].drop(indexToDelete)
                    if self.symbol_data[contract].empty:
                        del self.symbol_data[contract]
                        self.symbol_list.remove(contract)
                        continue
                    self.log_if("contract: %s - time serie start at: '%s' and ends at: '%s'" % (
                    contract, self.symbol_data[contract].index[0].date(), self.symbol_data[contract].index[-1].date()))
                    # Combine the index to pad forward values
                    if self.index is None:
                        self.index = self.symbol_data[contract].index
                    else:
                        self.index = self.index.union(self.symbol_data[contract].index)
                    # Set the latest symbol_data to None
                    self.latest_symbol_data[contract] = []
                else:
                    print("Symbol not found: %s in %s" % (contract, pd_data_symbol.keys()))
                    print(pd_data_history.head())

        self.index = pd.DataFrame(index=self.index)
        self.all_symbol_list = deepcopy(self.symbol_list)
        self.log_if("DataFrame start: '%s' - ends: '%s - Days: %s'" % (self.index.index[0].date(), self.index.index[-1].date(), len(self.index)))

    def _open_convert_csv_spot_files(self):
        """
        Opens the CSV files from the data directory, converting
        them into pandas DataFrames within a symbol dictionary.
        For this handler it will be assumed that the data is
        taken from ROFEX CEM. Thus its format will be respected.
        """
        symbols = dict()
        for s in self.symbol_list:
            if s[:3] not in symbols.keys():
                symbols[s[:3]] = []

        for underlying in symbols.keys():
            path = os.path.join(self.csv_dir, "%s_Spot.csv" % underlying)
            if os.path.exists(path):
                self.underlyings.append(underlying)
                dateparse = lambda x: datetime.strptime(x, '%d/%m/%Y 12:00:00 a.m.')
                pd_data_spot = pd.io.parsers.read_csv(path, header=0, index_col=0, parse_dates=['date'],
                                                      date_parser=dateparse, thousands=',',
                                                      names=["date", "type", "spot"]).sort_values("date",
                                                                                                  ascending=True)
                pd_data_spot["spot"] = pd_data_spot["spot"] / 1000000
                pd_data_spot.drop('type', axis=1, inplace=True)
                self.symbol_data[underlying] = pd_data_spot
                self.latest_symbol_data[underlying] = []
            else:
                print("Fail to open File:", path)

    def _open_convert_csv_files(self):
        """
        Opens the CSV files from the data directory, converting
        them into pandas DataFrames within a symbol dictionary.
        For this handler it will be assumed that the data is
        taken from ROFEX CEM. Thus its format will be respected.
        """
        symbols = dict()
        for s in self.symbol_list:
            if s[:3] not in symbols.keys():
                symbols[s[:3]] = []
            if len(s) > 3:
                symbols[s[:3]].append(s)
        self.symbol_list = []

        for underlying in symbols.keys():
            dateparse = lambda x: datetime.strptime(x, '%d/%m/%Y 12:00:00 a.m.')
            pd_data_history = pd.io.parsers.read_csv(os.path.join(self.csv_dir, "%s_History.csv" % underlying),
                                                     header=0,
                                                     index_col=0, parse_dates=['date'], date_parser=dateparse,
                                                     thousands=',',
                                                     names=["date", "contract", "type", "Ejercicio", "open", "low",
                                                            "high", "close", "volume", "settlement", "discount"]).sort_values("date", ascending=True)
            pd_data_history.drop('type', axis=1, inplace=True)
            pd_data_history.drop('Ejercicio', axis=1, inplace=True)
            pd_data_history.drop('discount', axis=1, inplace=True)
            pd_data_history["open"] = pd_data_history["open"] / 1000
            pd_data_history["low"] = pd_data_history["low"] / 1000
            pd_data_history["high"] = pd_data_history["high"] / 1000
            pd_data_history["close"] = pd_data_history["close"] / 1000
            pd_data_history["settlement"] = pd_data_history["settlement"] / 1000000

            contract_list = pd_data_history["contract"].unique()
            pd_data_symbol = dict()
            for contract in contract_list:
                if contract[:3] == underlying:
                    pd_data_symbol[contract.strip()] = pd_data_history[pd_data_history["contract"] == contract]

            if not symbols[underlying]:
                symbols[underlying] = list(pd_data_symbol.keys())
                self.symbol_list.extend(list(pd_data_symbol.keys()))
            else:
                self.symbol_list.append(symbols[underlying])

            #print("Instruments List: %s" % symbols[underlying])

            for contract in symbols[underlying]:
                if contract in pd_data_symbol.keys():
                    self.symbol_data[contract] = pd_data_symbol[contract]
                    self.symbol_data[contract] = self.symbol_data[contract].drop('contract', axis=1)
                    # Get names of indexes for which column Age has value 30
                    max_contract_date = self.get_contract_exp_date(contract)
                    indexToDelete = self.symbol_data[contract][self.symbol_data[contract].index > max_contract_date].index
                    # Delete these row indexes from dataFrame
                    self.symbol_data[contract] = self.symbol_data[contract].drop(indexToDelete)

                    if self.symbol_data[contract].empty:
                        del self.symbol_data[contract]
                        self.symbol_list.remove(contract)
                        continue

                    self.log_if("contract: %s - time serie start at: '%s' and ends at: '%s'" % (contract, self.symbol_data[contract].index[0].date(), self.symbol_data[contract].index[-1].date()))
                    # Combine the index to pad forward values
                    if self.index is None:
                        self.index = self.symbol_data[contract].index
                    else:
                        self.index = self.index.union(self.symbol_data[contract].index)
                    # Set the latest symbol_data to None
                    self.latest_symbol_data[contract] = []

                else:
                    print("Symbol not found: %s in %s" % (contract, pd_data_symbol.keys()))
                    print(pd_data_history.head())
        self.all_symbol_list = deepcopy(self.symbol_list)
        self.log_if("DataFrame start: '%s' - ends: '%s' - Days: %s" % (self.index[0].date(), self.index[-1].date(), len(self.index)))

    def _get_new_bar(self, symbol):
        """
        Returns the latest bar from the data feed.
        """
        if self.index_now.date() in self.symbol_data[symbol].index:
            return self.symbol_data[symbol].loc[self.index_now.date()]
        else:
            if self.symbol_data[symbol].index[0] < self.index_now <= self.symbol_data[symbol].index[-1]:
                idx = self.index.index.get_loc(self.index_now)
                prev_index = self.index.index[idx - 1]

                idx_s = self.symbol_data[symbol].index.get_loc(prev_index)
                post_index_s = self.symbol_data[symbol].index[idx_s + 1]
                nro_days = (post_index_s.date() - prev_index.date()).days
                rest = (nro_days // 7) * 2
                nro_days -= rest

                post_data = self.symbol_data[symbol].loc[post_index_s.date()]
                prev_data = self.symbol_data[symbol].loc[prev_index.date()]
                to_add = pd.DataFrame(prev_data).transpose()
                to_add.index = pd.DatetimeIndex([self.index_now])
                for (columnName, columnData) in to_add.iteritems():
                    if math.isnan(prev_data[columnName]) or math.isnan(post_data[columnName]):
                        to_add[columnName].iloc[0] = float('nan')
                    else:
                        add = post_data[columnName] - prev_data[columnName]
                        to_add[columnName].iloc[0] = round(prev_data[columnName] + (add/nro_days), 3)

                self.symbol_data[symbol] = self.symbol_data[symbol].append(to_add, sort=True).sort_index()
                #print("an inconsistency found in symbol: %s data. no value found for index: %s. used prev value: "
                #      "date: %s - value: %s" % (symbol, self.index_now.date(), prev_index.date(), prev_data["settlement"]))
                return self.symbol_data[symbol].loc[self.index_now.date()]
            else:
                print("index: %s do not exist for symbol: %s data. Index range for symbol: "
                      "%s to %s" % (self.index_now, symbol, self.symbol_data[symbol].index[0].date(),
                                    self.symbol_data[symbol].index[-1].date()))

    def _update_bars(self):
        """
        Pushes the latest bar to the latest_symbol_data structure
        for all symbols in the symbol list.
        """

        # Update Symbols Data
        for s in self.active_symbols[self.index_now]:
            bar = self._get_new_bar(s)
            if bar is not None:
                self.latest_symbol_data[s].append(bar)
        # Update Spot Data
        for u in self.underlyings:
            bar = self._get_new_bar(u)
            if bar is not None:
                self.latest_symbol_data[u].append(bar)

    def _update_valid_symbol_list(self):
        self.active_symbols[self.index_now] = self.active_symbols[self.index_now]

        for symbol in self.active_symbols[self.index_now]:
            if self.index_now >= self.symbol_data[symbol].index[-1]:
                self.active_symbols[self.index_now].remove(symbol)

        for symbol in self.symbol_list:
            if symbol in self.symbol_data.keys():
                if self.symbol_data[symbol].index[0] <= self.index_now < self.symbol_data[symbol].index[-1]:
                    self.active_symbols[self.index_now].append(symbol)
                    self.symbol_list.remove(symbol)
            else:
                print("error, no symbol process like: %s" % symbol)

    def get_latest_bar(self, symbol):
        """
        Returns the last bar from the latest_symbol list.
        """
        if symbol in self.latest_symbol_data.keys():
            if self.latest_symbol_data[symbol]:
                return self.latest_symbol_data[symbol][-1]
            else:
                return None
        else:
            print("That symbol is not available in the historical data set.")
            return None

    def get_latest_bars(self, symbol, N=1):
        """
        Returns the last N bars from the latest_symbol list,
        or N-k if less available.
        """
        if symbol in self.latest_symbol_data.keys():
            if self.latest_symbol_data[symbol]:
                return self.latest_symbol_data[symbol][-N:]
            else:
                return []
        else:
            print("That symbol is not available in the historical data set.")
            return None

    def get_latest_bar_datetime(self, symbol):
        """
        Returns a Python datetime object for the last bar.
        """
        if symbol in self.latest_symbol_data.keys():
            if self.latest_symbol_data[symbol]:
                return self.latest_symbol_data[symbol][-1][0]
            else:
                return None
        else:
            print("That symbol is not available in the historical data set.")
            return None

    def get_latest_bar_value(self, symbol, val_type):
        """
        Returns one of the Open, High, Low, Close, Volume or OI
        values from the pandas Bar series object.
        """
        if symbol in self.latest_symbol_data.keys():
            if self.latest_symbol_data[symbol]:
                return getattr(self.latest_symbol_data[symbol][-1], val_type)
            else:
                return None
        else:
            print("That symbol is not available in the historical data set.")
            return None

    def get_latest_bars_values(self, symbol, val_type, N=1):
        """
        Returns the last N bar values from the
        latest_symbol list, or N-k if less available.
        """
        if symbol in self.latest_symbol_data.keys():
            if self.latest_symbol_data[symbol]:
                bars_list = self.get_latest_bars(symbol, N)
                return np.array([getattr(b, val_type) for b in bars_list])
            else:
                return []
        else:
            print("That symbol is not available in the historical data set.")
            return None

    def next(self, send_event=True):
        idx = self.index.index.get_loc(self.index_now)
        if (idx + 1) < self.index.shape[0] and self.index_now <= self.end_date:
            self.trading_days_history_count += 1
            self.index_now = self.index.index[idx + 1]
            self._update_valid_symbol_list()
            self._update_bars()
            if send_event:
                self.events.put(MarketEvent())
        else:
            self.log_if("Finalizing Backtesting. Day: %s" % self.index_now)
            self.continue_backtest = False

    def get_contract_exp_date(self, contract):
        month = int(contract[3:5])
        year = int(contract[5:9])
        day = calendar.monthrange(year, month)[1]
        return datetime(year, month, day)

    def get_trading_days_history(self):
        return self.trading_days_history_count

    def get_trading_days_to_expire(self, contract):
        if contract in self.active_symbols[self.index_now]:
            return self.symbol_data[contract][self.symbol_data[contract].index > self.index_now].shape[0]

    def get_active_symbols(self):
        return self.active_symbols[self.index_now]

    def get_day(self):
        return self.index_now.date()

    def get_contract_size(self, contract):
        if contract in self.all_symbol_list:
            return self.contract_size[contract[:3]]
        elif contract in self.contract_size:
            return self.contract_size[contract]

    def log_if(self, msg):
        if self.logging:
            print(msg)