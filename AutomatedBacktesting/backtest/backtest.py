#coding=utf-8

from __future__ import print_function

try:
    import Queue as queue
except ImportError:
    import queue
import time
import pprint


class Backtest(object):
    """
    Enscapsulates the settings and components for carrying out
    an event-driven backtest.
    """
    def __init__(self, csv_dir, symbol_list, contract_sizes, initial_capital,
                heartbeat, start_date, end_date, data_handler,
                execution_handler, portfolio, strategy, params, logging):
        """
        Initialises the backtest.
        Parameters:
        csv_dir - The hard root to the CSV data directory.
        symbol_list - The list of symbol strings.
        intial_capital - The starting capital for the portfolio.
        heartbeat - Backtest "heartbeat" in seconds
        start_date - The start datetime of the strategies.
        data_handler - (Class) Handles the market data feed.
        execution_handler - (Class) Handles the orders/fills for trades.
        portfolio - (Class) Keeps track of portfolio current and prior positions.
        strategies - (Class) Generates signals based on market data.
        """
        self.csv_dir = csv_dir
        self.logging = logging
        self.symbol_list = symbol_list
        self.contract_sizes = contract_sizes
        self.initial_capital = initial_capital
        self.heartbeat = heartbeat
        self.start_date = start_date
        self.end_date = end_date
        self.data_handler_cls = data_handler
        self.execution_handler_cls = execution_handler
        self.portfolio_cls = portfolio
        self.strategy_cls = strategy
        self.strategy_params = params
        self.events = queue.Queue()
        self.signals = 0
        self.orders = 0
        self.fills = 0
        self.num_strats = 1

        self.count = dict()
        self.timer = dict()
        self.timers_names = ["data_handler.next", "_generate_trading_instances", 'portfolio.update_fill',
                             'execution_handler.execute_order', 'portfolio.update_signal',
                             'portfolio.update_timeindex', 'strategies.calculate_signals']
        self._init_timer()

        self.timeit(self._generate_trading_instances, None, '_generate_trading_instances')

    def timeit(self, method, param, name):
        start = time.process_time()
        if param:
            method(param)
        else:
            method()
        self.timer[name] += time.process_time() - start
        self.count[name] += 1

    def _init_timer(self):
        for name in self.timers_names:
            self.count[name] = 0
            self.timer[name] = 0.0

    def _generate_trading_instances(self):
        """
        Generates the trading instance objects from
        their class types.
        """
        self.log_if("Creating DataHandler, Strategy, Portfolio and ExecutionHandler")
        self.data_handler = self.data_handler_cls(self.events, self.csv_dir, self.symbol_list,
                                                  self.contract_sizes, self.start_date, self.end_date, self.logging)
        self.strategy = self.strategy_cls(self.data_handler, self.events, self.strategy_params)
        self.portfolio = self.portfolio_cls(self.data_handler, self.events,self.start_date,self.initial_capital)
        self.execution_handler = self.execution_handler_cls(self.events)

    def reset_trading_instances(self, start_date, end_date, params):
        self.log_if("Reseting DataHandler, Strategy, Portfolio and ExecutionHandler")
        self.start_date = start_date
        self.end_date = end_date
        self.strategy_params = params
        self.events.queue.clear()
        self.data_handler.reset(start_date, end_date)
        self.strategy = self.strategy_cls(self.data_handler, self.events, self.strategy_params)
        self.portfolio = self.portfolio_cls(self.data_handler, self.events, self.start_date, self.initial_capital)
        self.execution_handler = self.execution_handler_cls(self.events)

    def log_if(self, msg):
        if self.logging:
            print(msg)

    def _run_backtest(self):
        """
        Executes the backtest.
        """
        self.log_if("Start Backtesting")
        i = 0
        while True:
            i += 1
            # Update the market bars
            if self.data_handler.continue_backtest:
                self.timeit(self.data_handler.next, None, 'data_handler.next')
                #print("iteration: %s" % (i))
            else:
                break
            # Handle the events
            while True:
                try:
                    event = self.events.get(False)
                except queue.Empty:
                    break
                else:
                    if event is not None:
                        if event.type == "MARKET":
                            self.timeit(self.strategy.calculate_signals, event, 'strategies.calculate_signals')
                            self.timeit(self.portfolio.update_timeindex, event, 'portfolio.update_timeindex')
                        elif event.type == "SIGNAL":
                            self.signals += 1
                            self.timeit(self.portfolio.update_signal, event, 'portfolio.update_signal')
                        elif event.type == "ORDER":
                            self.orders += 1
                            self.timeit(self.execution_handler.execute_order, event, 'execution_handler.execute_order')
                        elif event.type == "FILL":
                            self.fills += 1
                            self.timeit(self.portfolio.update_fill, event, 'portfolio.update_fill')
            time.sleep(self.heartbeat)

    def _output_performance(self, short=False):
        """
        Outputs the strategies performance from the backtest.
        """

        self.portfolio.create_equity_curve_dataframe()
        self.log_if("Creating summary stats...") if not short else None
        stats = self.portfolio.output_summary_stats()
        self.log_if("Creating equity curve...") if not short else None
        self.log_if(self.portfolio.equity_curve.tail(10))
        if short:
            print(stats)
        else:
            pprint.pprint(stats)
        self.log_if("Signals: %s" % self.signals) if not short else None
        self.log_if("Orders: %s" % self.orders) if not short else None
        self.log_if("Fills: %s" % self.fills) if not short else None

    def _print_timer(self):
        for name in self.timers_names:
            mean = 0 if self.count[name] == 0 else (self.timer[name] / self.count[name])
            self.log_if("case %s - count: %s - Time -> total: %s - mean: %s" %
                        (name, self.count[name], self.timer[name], mean))

        self.strategy.print_timer()

    def simulate_trading(self):
        """
        Simulates the backtest and outputs portfolio performance.
        """

        self._run_backtest()
        #self._print_timer()
        self._output_performance(True)