import os

try:
    import Queue as queue
except ImportError:
    import queue

from AutomatedBacktesting.backtest.backtest import Backtest
from AutomatedBacktesting.dataHandler.historicCSVDataHandler import HistoricCSVDataHandler
from AutomatedBacktesting.executionHandler.simulatedExecutionHandler import SimulatedExecutionHandler
from AutomatedBacktesting.strategies.strategy_calendar_spread import CalendarSpreadStrategy
from AutomatedBacktesting.strategies.strategy_day_momentum import DayMomentumStrategy
from AutomatedBacktesting.portfolio.portfolio import Portfolio

csv_dir = os.getcwd() + "/historic_csv/rawData"
symbol_list = ["DLR"]
initial_capital = 10000
heartbeat = 0
contract_sizes = {"DLR": 1000}
events = queue.SimpleQueue()
execution_handler = SimulatedExecutionHandler
data_handler = HistoricCSVDataHandler
portfolio = Portfolio
logging = False

#start_date = "2010-01-23"
start_date = "2000-01-01"
end_date = "2010-12-31"

strategy = DayMomentumStrategy

params_list = []
# Configure Strategy
if strategy is CalendarSpreadStrategy:
    for lookback in [21, 42, 63, 84, 105, 126]:
        for holding in [10, 21, 31, 42, 52, 63, 73, 84]:
            for months in [3, 5, 7, 9, 12]:
                params = [holding, lookback, 5, months, "DLR"]
                params_list.append(params)
elif strategy is DayMomentumStrategy:
    for lookback in [25, 50, 75, 100, 125]:
        for holding in [1, 5, 60, 120, 150]:
            params = [lookback, holding, "DLR"]
            params_list.append(params)
#("", "")
#periods = [("2003-10-01", "2007-10-01"), ("2007-10-01", "2011-10-01"),
#           ("2011-10-01", "2015-10-01"), ("2015-10-01", "2019-10-01")]
periods = [("2015-10-01", "2019-10-01")]
#p = [10, 21, 5, 7, 'DLR']


for (s, e) in periods:
    bt = None
    start_date = s
    end_date = e
    for params in params_list:
        params = [25, 5, 'DLR']
        print("Running Backtest for %s with Parameters: %s" % (strategy.__name__, params))
        if bt:
            bt.reset_trading_instances(start_date, end_date, params)
        else:
            bt = Backtest(csv_dir, symbol_list, contract_sizes, initial_capital, heartbeat, start_date, end_date,
                          data_handler, execution_handler, portfolio, strategy, params, logging)
        bt.simulate_trading()
        bt.portfolio.print_equity_curve(s, e)
        break